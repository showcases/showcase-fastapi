# -*- coding: utf-8 -*-
"""Dungeons and Dragons dice roller API module."""

from typing import Tuple

from decouple import config as env_config
from fastapi import FastAPI

from .config import config

VERSION: Tuple[int, int, int] = (0, 1, 0)
__version__ = ".".join(map(str, VERSION))


app: FastAPI = FastAPI()
HOST: str = env_config("HOST", default="127.0.0.1")
PORT: int = env_config("PORT", default=8000, cast=int)
config(app)
