# -*- coding: utf-8 -*-
import click
import uvicorn

from . import HOST, PORT, app


@click.command()
@click.option(
    "-p",
    "--port",
    help="what port to run the api server on",
    type=click.INT,
    default=PORT,
)
def server(port: int) -> None:
    """Start local FastApi GrahpQL server"""
    uvicorn.run(app, port=port, host=HOST)
