# -*- coding: utf-8 -*-
"""
Dice Roller service module.
This module holds all the essential functionalities for making a dice roll.
"""

from typing import Dict, Tuple

VERSION: Tuple[int, int, int] = (0, 1, 0)
__version__ = ".".join(map(str, VERSION))

DICE_ROLL_INSTRUCTION_PATTERN: str = (
    r"^(\d+)?([dD](4|6|8|100|10|12|20))(([bBwW])(\d+))?([+-]\d+)?$"
)
BEST: str = "b"

# all group of intrerest from dice roll instruction regex pattern
DICE_ROLL_INSTRUCTION_GROUPS: Dict[str, int] = dict(
    string=0,  # dice roll full instruction string
    attempts=1,  # dice roll instruction attempt group
    die=3,  # dice roll instruction die group
    best=5,  # dice roll instruction best group
    choose=6,  # dice roll instruction choose group
    modifier=7,  # dice roll instruction modifier group
)
