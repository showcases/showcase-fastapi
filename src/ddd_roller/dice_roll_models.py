# -*- coding: utf-8 -*-
from typing import List

from pydantic import UUID4, PositiveInt, conint, validator
from pydantic.main import BaseModel


# noinspection PyMethodParameters
class DiceInstruction(BaseModel):
    string: str
    die: conint(ge=4, le=100, multiple_of=2)  # type: ignore
    attempts: PositiveInt = 1  # type: ignore
    choose: PositiveInt = 0  # type: ignore
    best: bool = True
    modifier: int = 0

    @validator("choose")
    def choose_must_be_le_attempts(cls, value: int, values: dict) -> int:
        if value > values["attempts"]:
            raise ValueError(
                "the amount of die to be chosen is greater than the attempts"
            )
        return value

    @validator("die")
    def dice_size(cls, value: int) -> int:
        if value > 12 and value not in [20, 100]:
            raise ValueError(
                f"die: {value} is over 12, but is neither 20 or 100",
            )
        return value


# noinspection PyMethodParameters
class DiceRoll(BaseModel):
    instruction: DiceInstruction
    rolls: List[conint(strict=True, gt=0, le=100)]  # type: ignore

    @property
    def total(self) -> int:
        return sum(self.picks) + self.instruction.modifier

    @property
    def picks(self) -> List[int]:
        if not self.instruction.choose or self.instruction.choose == len(self.rolls):
            return self.rolls
        return sorted(self.rolls, reverse=self.instruction.best)[
            : self.instruction.choose
        ]

    @validator("rolls")
    def rolls_must_match_instruction(cls, value: List[int], values: dict) -> List[int]:
        if len(value) != values["instruction"].attempts:
            raise ValueError("Rolls does not match dice roll instruction.")
        return value

    @validator("rolls")
    def rolls_highest_roll_must_be_le_to_die(
        cls, value: List[int], values: dict
    ) -> List[int]:
        if max(value) > values["instruction"].die:
            raise ValueError("Item in Rolls object can not be larger than die size")
        return value
