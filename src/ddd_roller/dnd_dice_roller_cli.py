#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from json import dumps

import click

from .instruction_service import create_dice_roll_instruction
from .roll_service import roll_dice_by_instruction


@click.command()
@click.option("-v", "--verbose", help="increase output verbosity", is_flag=True)
@click.argument("instruction")
def cli(instruction: str, verbose: bool = False):
    """Welcome to ddd_roller a Dungeons & Dragons (DnD) Dice Roller CLI.

    Given an DnD dice instruction string, 'ddd_roller' will parse, execute and return a json with the dice roll result.

    example: 4d6b3+1 -> { 'instruction': '4d6b3+1', 'picks': [5,5,4], 'value': 15 }
    """
    if verbose:
        dice_instruction = create_dice_roll_instruction(instruction=instruction)
        click.echo(
            f"instruction was parsed as followed -> {dumps(dict(dice_instruction), indent=2)}"
        )
        dice_roll = roll_dice_by_instruction(instruction=dice_instruction)
        click.echo(f"All rolls -> {dice_roll.rolls}")
        click.echo(
            f"{'best' if dice_roll.instruction.best else 'worst'} rolls were picked"
        )
        picks = dice_roll.picks
        click.echo(f"All picks {picks}")
        click.echo(
            f"value of the roll is sum({picks}) + {dice_roll.instruction.modifier} = {dice_roll.total}"
        )
    else:
        dice_roll = roll_dice_by_instruction(instruction=instruction)
        click.echo(
            dumps(
                {
                    "instruction": dice_roll.instruction.string,
                    "picks": dice_roll.picks,
                    "value": dice_roll.total,
                }
            )
        )


if __name__ == "__main__":
    cli()
