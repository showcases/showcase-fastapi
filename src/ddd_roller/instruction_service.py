# -*- coding: utf-8 -*-
"""
Dice Roll instruction service module.
Given an input string, this module has the tools to process it to a legable model.
"""

from typing import Dict, Match, Union

from regex import match

from . import BEST, DICE_ROLL_INSTRUCTION_GROUPS, DICE_ROLL_INSTRUCTION_PATTERN
from .dice_roll_models import DiceInstruction


def parse_instruction_string(instruction: str) -> Match:
    """parse input string by regular expression.

    Args:
        instruction (str): dice roll instruction string.

    Raises:
        ValueError: if no instruction is given (empty string)
        ValueError: if dice roll instruction is invalid.

    Returns:
        Match: regular expression match object
    """
    if instruction is None or not instruction:
        raise ValueError("An instruction must be given.")

    if match_groups := match(pattern=DICE_ROLL_INSTRUCTION_PATTERN, string=instruction):
        return match_groups
    raise ValueError(f"'{instruction}' is not an applicable dice roll instruction")


def _create_dice_roll_instruction(match: Match) -> Dict[str, Union[str, bool]]:
    """Create a dict with the keys inline with a DiceInstruction model.

    Args:
        match (Match): regular expression match object

    Returns:
        Dict[str, Union[str, bool]]
    """
    return {
        key: (value if key != "best" else value[0].lower() == BEST)
        for key, group in DICE_ROLL_INSTRUCTION_GROUPS.items()
        if (value := match.group(group))
    }


def create_dice_roll_instruction(instruction: str) -> DiceInstruction:
    """Create a DiceRollInstruction model from dice roll instruction string.

    Args:
        instruction (str): dice roll instruction string

    Returns:
        DiceInstruction: pydantic model
    """
    instruction_match: Match = parse_instruction_string(instruction=instruction)
    return DiceInstruction(**_create_dice_roll_instruction(match=instruction_match))
