# -*- coding: utf-8 -*-
from random import randrange
from typing import List, Union

from .dice_roll_models import DiceInstruction, DiceRoll
from .instruction_service import create_dice_roll_instruction


def roll_die(die: int, attempts: int) -> List[int]:
    return [randrange(1, die) for _ in range(attempts)]


def roll_dice_by_instruction(instruction: Union[str, DiceInstruction]) -> DiceRoll:
    """Generate a DiceRoll from an instruction string or pydantic DiceInstruction model

    Args:
        instruction (Union[str, DiceInstruction]): instruction string | pydantic DiceInstruction

    Returns:
        DiceRoll: pydantic model
    """
    if isinstance(instruction, str):
        instruction = create_dice_roll_instruction(instruction=instruction)
    return DiceRoll(
        instruction=instruction,
        rolls=roll_die(**instruction.dict(include={"die", "attempts"})),
    )


def re_roll_dice_roll(dice_roll: DiceRoll) -> None:
    dice_roll.rolls = roll_die(
        **dice_roll.instruction.dict(include={"die", "attempts"})
    )
