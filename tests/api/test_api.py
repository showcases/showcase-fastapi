# -*- coding: utf-8 -*-
from src.api import HOST, PORT, __version__


def test_version() -> None:
    assert __version__ == "0.1.0"


def test_default_port() -> None:
    assert PORT == 8000


def test_default_host() -> None:
    assert HOST == "127.0.0.1"
