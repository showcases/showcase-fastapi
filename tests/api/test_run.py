# -*- coding: utf-8 -*-
from typing import List

import pytest
import uvicorn

from src.api.run import server
from tests.fixures import cli_runner, mock_uvicorn  # noqa: F401


# TODO: need a more relevant tests
@pytest.mark.parametrize(
    "args",
    (["--port", "5000"], ["-p", "5000"], [], ["--help"]),
)
def test_server(args: List[str], cli_runner, mock_uvicorn) -> None:  # noqa: F811
    result = cli_runner.invoke(server, args)
    assert not result.exception
