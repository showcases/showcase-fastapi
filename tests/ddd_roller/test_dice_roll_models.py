# -*- coding: utf-8 -*-
import pytest
from pydantic import ValidationError

from src.ddd_roller.dice_roll_models import DiceInstruction, DiceRoll
from tests.fixures import dice_instruction_4d20b3


@pytest.mark.parametrize(
    "values, error, error_pattern",
    [
        (
            {"choose": 2, "die": 8},
            ValueError,
            "the amount of die to be chosen is greater than the attempts",
        ),
        (
            {"attempts": 1, "choose": 2, "die": 8},
            ValueError,
            "the amount of die to be chosen is greater than the attempts",
        ),
        (
            {"die": 2},
            ValidationError,
            "ensure this value is greater than or equal to 4",
        ),
        ({"die": 5}, ValidationError, "ensure this value is a multiple of 2"),
        (
            {"die": 101},
            ValidationError,
            "ensure this value is less than or equal to 100",
        ),
        (
            {"die": 10, "choose": -2},
            ValidationError,
            "ensure this value is greater than 0",
        ),
        (
            {"die": 10, "attempts": -2},
            ValidationError,
            "ensure this value is greater than 0",
        ),
        ({"die": 30}, ValueError, r"die: \d+ is over 12, but is neither 20 or 100"),
    ],
)
def test_dice_instruction(values: dict, error, error_pattern: str) -> None:
    with pytest.raises(expected_exception=error, match=error_pattern):
        DiceInstruction(string="some string", **values)
