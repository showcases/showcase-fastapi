# -*- coding: utf-8 -*-
import pytest
from hypothesis import example, given
from hypothesis.strategies import text

from src.ddd_roller.dice_roll_models import DiceInstruction

from src.ddd_roller.instruction_service import (  # isort:skip
    create_dice_roll_instruction,
)


@given(text())
@example(string="")
@example(string="sha")
def test_create_dice_roll_instruction_raises_error(string: str) -> None:
    with pytest.raises(
        ValueError,
    ):
        create_dice_roll_instruction(string)


@pytest.mark.parametrize(
    "instruction, expected",
    [
        ("d6", DiceInstruction(die=6, string="d6")),
        ("D6", DiceInstruction(die=6, string="D6")),
        ("2d6", DiceInstruction(die=6, attempts=2, string="2d6")),
        ("3d8b2", DiceInstruction(die=8, attempts=3, choose=2, string="3d8b2")),
        ("3d8B2", DiceInstruction(die=8, attempts=3, choose=2, string="3d8B2")),
        (
            "3d8w2",
            DiceInstruction(die=8, attempts=3, choose=2, best=False, string="3d8w2"),
        ),
        ("d10-2", DiceInstruction(die=10, modifier=-2, string="d10-2")),
        (
            "3d8b2+3",
            DiceInstruction(die=8, attempts=3, choose=2, modifier=3, string="3d8b2+3"),
        ),
    ],
)
def test_create_dice_roll_instruction(
    instruction: str, expected: DiceInstruction
) -> None:
    assert create_dice_roll_instruction(instruction=instruction) == expected
