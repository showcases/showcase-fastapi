# -*- coding: utf-8 -*-
from typing import List

import pytest

from src.ddd_roller.dnd_dice_roller_cli import cli
from tests.fixures import cli_runner  # noqa: F401


# TODO: need a more relevant tests
@pytest.mark.parametrize(
    "args",
    (["--verbose", "4d20b3"], ["-v", "4d20b3"], ["4d20b3"], ["--help"]),
)
def test_cli(args: List[str], cli_runner) -> None:  # noqa: F811
    result = cli_runner.invoke(cli, args)
    assert not result.exception
