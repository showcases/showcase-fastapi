# -*- coding: utf-8 -*-
import pytest
import uvicorn
from click.testing import CliRunner

from src.ddd_roller.dice_roll_models import DiceInstruction


@pytest.fixture
def dice_instruction_4d20b3() -> DiceInstruction:
    return DiceInstruction(string="4d20b3", die=20, attempts=4, choose=3)


@pytest.fixture
def cli_runner() -> CliRunner:
    return CliRunner()


class MockUvicorn:
    @staticmethod
    def run():
        return 0


@pytest.fixture
def mock_uvicorn(monkeypatch):
    def mock_run(*args, **kwargs):
        return MockUvicorn()

    monkeypatch.setattr(uvicorn, "run", mock_run)
